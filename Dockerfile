FROM bash AS registrator
RUN apk add --no-cache curl

ADD ./registrator .

CMD bash register.sh
